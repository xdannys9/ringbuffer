package mutable

import java.util.concurrent.atomic.AtomicReference

import scala.reflect.ClassTag

case class RingBufferValues(front: Int, rear: Int)

case class ArrayRingBufferCAS[A](capacity: Int, array: Array[A]) {

  private val ringBufferSettings = new AtomicReference[RingBufferValues](RingBufferValues(-1, 0))

  def size(ringBufferValues: RingBufferValues): Int = {
    ringBufferValues.rear - ringBufferValues.front
  }

  def enqueue(value: A): Boolean = {

    var compareAndSetResult = false
    var result = false

    while(!compareAndSetResult) {
      val ringBufferValues = ringBufferSettings.get()
      val rearArrayValue = (ringBufferValues.rear + 1) % array.length
      if (!(size(ringBufferValues) >= capacity)) {
        val frontValue =
          if(ringBufferValues.front == -1) 0
          else ringBufferValues.front
        compareAndSetResult = ringBufferSettings
          .compareAndSet(ringBufferValues, RingBufferValues(frontValue, rearArrayValue))
        if(compareAndSetResult) {

          array.update(ringBufferValues.rear, value)
        }
        result = true
      }
      else {
        compareAndSetResult = true
        result = false
      }
    }

    result
  }

  def dequeue(): Option[A] = {
    var compareAndSetResult = false
    var result: Option[A] = None
    while (!compareAndSetResult) {

      val ringBufferValues = ringBufferSettings.get()
      if (ringBufferValues.front == -1) {
        compareAndSetResult = true
        result = None
      } else if(ringBufferValues.rear == ringBufferValues.front) {
        val tmp = array(ringBufferValues.front % array.length)
        compareAndSetResult = ringBufferSettings
          .compareAndSet(ringBufferValues , RingBufferValues(-1, 0))
        result = Some(tmp)
      } else {
        val tmp = array(ringBufferValues.front % array.length)
        val newFront = ringBufferValues.front + 1
        compareAndSetResult = ringBufferSettings
          .compareAndSet(ringBufferValues ,RingBufferValues(newFront, ringBufferValues.rear))
        result = Some(tmp)
      }
    }
    result
  }

}

object ArrayRingBufferCAS {

  def empty[A: ClassTag](capacity: Int): ArrayRingBufferCAS[A] = {
    val array = Array.ofDim[A](capacity)
    ArrayRingBufferCAS[A](capacity, array)
  }

  def apply[A: ClassTag](capacity: Int)(els: A*): ArrayRingBufferCAS[A] = {
    val elements = if (els.size <= capacity) els else els.takeRight(capacity)
    val array = Array.ofDim[A](capacity)
    val arrayRingBuffer = ArrayRingBufferCAS[A](capacity, array)
    elements.foreach(el => arrayRingBuffer.enqueue(el))
    arrayRingBuffer
  }

}
