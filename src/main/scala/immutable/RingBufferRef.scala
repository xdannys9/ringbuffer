package immutable

import java.util.concurrent.atomic.AtomicReference

import scala.collection.immutable.Queue

case class RingBufferRef[A] private (capacity: Int, queue: AtomicReference[IQueue[A]]) {

  def enqueue(element: A): Boolean = {
    val queueReference = queue.get()
    if (queueReference.size < capacity) {
      queue.set(IQueue(
        queueReference.size + 1, queueReference.queue.enqueue(element)))
      true
    }
    else false
  }

  def dequeue: Option[A] = {
    val queueReference = queue.get()
    queueReference.queue.dequeueOption match {
      case Some((el, newQueue)) =>
        queue.set(IQueue(queueReference.size - 1, newQueue))
        Some(el)
      case None =>
        None
    }
  }
}

object RingBufferRef {

  def empty[A](capacity: Int): RingBufferRef[A] = RingBufferRef(capacity, new AtomicReference[IQueue[A]](
    IQueue(0, Queue.empty[A])
  ))

  def apply[A](capacity: Int)(els: A*): RingBufferRef[A] = {
    val elements = if (els.size <= capacity) els else els.takeRight(capacity)
    RingBufferRef(capacity, new AtomicReference[IQueue[A]](
      IQueue(elements.size, Queue(elements: _*))))
  }

}