package immutable

import java.util.concurrent.atomic.AtomicReference

import scala.collection.immutable.Queue

case class IQueue[A] (
                       size: Int,
                       queue: Queue[A]
                     )

case class RingBufferCAS[A] private (capacity: Int, queue: AtomicReference[IQueue[A]]) {

  def enqueue(element: A): Boolean = {
    var compareAndSetResult = false
    var result = false
    while (!compareAndSetResult) {
      val queueReference = queue.get()
      if (queueReference.size < capacity) {
        compareAndSetResult = queue.compareAndSet(queueReference, IQueue(
          queueReference.size + 1, queueReference.queue.enqueue(element)))
        result = true
      } else {
        compareAndSetResult = true
        result = false
      }
    }
    result
  }

  def dequeue: Option[A] = {
    var compareAndSetResult = false
    var result: Option[A] = None
    while (!compareAndSetResult) {
      val queueReference = queue.get()
      queueReference.queue.dequeueOption match {
        case Some((el, newQueue)) => {
          compareAndSetResult = queue.compareAndSet(queueReference, IQueue(queueReference.size - 1, newQueue))
          result = Some(el)
        }
        case None =>
          {
            compareAndSetResult = true
            result = None
          }
      }
    }
    result
  }
}

object RingBufferCAS {

  def empty[A](capacity: Int): RingBufferCAS[A] = RingBufferCAS(capacity, new AtomicReference[IQueue[A]](
    IQueue(0, Queue.empty[A])
  ))

  def apply[A](capacity: Int)(els: A*): RingBufferCAS[A] = {
    val elements = if (els.size <= capacity) els else els.takeRight(capacity)
    RingBufferCAS(capacity, new AtomicReference[IQueue[A]](
      IQueue(elements.size, Queue(elements: _*))))
  }

}