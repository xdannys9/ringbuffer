package immutable

import scala.collection.immutable.Queue

case class RingBuffer[+A] private (size: Int, capacity: Int, queue: Queue[A]) {

  def enqueue[B >: A](element: B): (Option[A], RingBuffer[B]) = {
    if (size < capacity) None -> RingBuffer(capacity, size + 1, queue.enqueue(element))
    else queue.dequeue match {
      case (el, newQueue) => Some(el) -> RingBuffer(capacity, size, newQueue.enqueue(element))
    }
  }

  def dequeue: (Option[A], RingBuffer[A]) = {
    queue.dequeueOption match {
      case Some((el, newQueue)) => Some(el) -> RingBuffer(capacity, size - 1, newQueue)
      case None => None -> RingBuffer(capacity, size, queue)
    }
  }
}

object RingBuffer {

  def empty[A](capacity: Int): RingBuffer[A] = RingBuffer(0, capacity, Queue.empty[A])

  def apply[A](capacity: Int)(els: A*): RingBuffer[A] = {
    val elements = if (els.size <= capacity) els else els.takeRight(capacity)
    RingBuffer(elements.size, capacity, Queue(elements: _*))
  }

}
