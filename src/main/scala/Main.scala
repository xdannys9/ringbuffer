import mutable.{ArrayRingBufferCAS}

object Main extends App {
  def cube(x: Int) = {
    x * x * x
  }



  val newArrayBuffer = ArrayRingBufferCAS.empty[Int](10)
//  val newArrayBuffer = immutable.RingBuffer.empty[Int](10)
  println("HERE")

  val range = Range(0, 20)
    .map{ int =>
    print(int)
    println(newArrayBuffer.enqueue(int))
      println(newArrayBuffer.array.foreach(int => print(int + " ")))
//    println(newArrayBuffer.dequeueOption())
  }

  val range2 = Range(0, 20)
    .map{ int =>
      print(int)
      println(newArrayBuffer.dequeueOption)
      println(newArrayBuffer.array.foreach(int => print(int + " ")))
      //    println(newArrayBuffer.dequeueOption())
    }


}
