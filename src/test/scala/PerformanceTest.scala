import java.io.File
import java.util.concurrent.{ArrayBlockingQueue, ConcurrentLinkedDeque, ForkJoinPool, LinkedBlockingQueue}

import mutable.ArrayRingBufferCAS
import immutable.{RingBufferCAS, RingBufferSynchronized}
import org.scalameter.api._
import org.scalameter.picklers.Implicits._
import org.scalameter.Bench.OfflineReport

import scala.collection.parallel.ForkJoinTaskSupport


object PerformanceTest extends OfflineReport {

  override val executor = SeparateJvmsExecutor(
    new Executor.Warmer.Default,
    Aggregator.min,
    new Measurer.Default
  )

  override val reporter: Reporter[Double] = Reporter.Composite(
    new RegressionReporter(
      RegressionReporter.Tester.OverlapIntervals(),
      RegressionReporter.Historian.ExponentialBackoff() ),
    HtmlReporter(true)
  )

  override def persistor: Persistor = JSONSerializationPersistor(new File("target/benchmarks/sun"))

//  override val reporter = ChartReporter(ChartFactory.XYLine())

  val sizes = Gen.range("size")(300000, 1500000, 500000)
  val taskSupport = new ForkJoinTaskSupport(new ForkJoinPool(5))

  val ranges = for {
    size <- sizes
  } yield 0 until size

  performance of "ArrayRingBufferCAS" in {
    measure method "enqueue" in {
      using(ranges) in {
        r =>
          val arrayRingBuffer = ArrayRingBufferCAS.empty[Int](r.size)
          val parallel = r.par
          parallel.tasksupport = taskSupport
          parallel.map{ i => arrayRingBuffer.enqueue(i)}
      }
    }
  }

  performance of "immutable.RingBufferCAS" in {
    measure method "enqueue" in {
      using(ranges) in {
        r =>
          val ringBuffer = RingBufferCAS.empty[Int](r.size)
          val parallel = r.par
          parallel.tasksupport = taskSupport
          parallel.map{ i => ringBuffer.enqueue(i)}
      }
    }
  }

  performance of "immutable.RingBufferSynchronized" in {
    measure method "enqueue" in {
      using(ranges) in {
        r =>
          val ringBuffer = RingBufferSynchronized.empty[Int](r.size)
          val parallel = r.par
          parallel.tasksupport = taskSupport
          parallel.map{ i => ringBuffer.enqueue(i)}
      }
    }
  }

  performance of "ConcurrentLinkedQueue" in {
    measure method "enqueue" in {
      using(ranges) in {
        r =>
          val linkedQueue = new ConcurrentLinkedDeque[Int]()
          val parallel = r.par
          parallel.tasksupport = taskSupport
          parallel.map{ i =>
            linkedQueue.add(i)
          }
      }
    }
  }

  performance of "LinkedBlockingQueue" in {
    measure method "enqueue" in {
      using(ranges) in {
        r =>
          var linkedQueue = new LinkedBlockingQueue[Int]()
          val parallel = r.par
          parallel.tasksupport = taskSupport
          parallel.map{ i =>
            linkedQueue.add(i)
          }
      }
    }
  }

  performance of "ArrayBlockingQueue" in {
    measure method "enqueue" in {
      using(ranges) in {
        r =>
          var linkedQueue = new ArrayBlockingQueue[Int](r.size)
          val parallel = r.par
          parallel.tasksupport = taskSupport
          parallel.map{ i =>
            linkedQueue.add(i)
          }
      }
    }
  }

  performance of "ArrayRingBufferCAS" in {
    measure method "dequeue" in {
      using(ranges) in {
        r =>
          val arrayRingBuffer = ArrayRingBufferCAS.empty[Int](r.size)
          val parallel = r.par
          parallel.tasksupport = taskSupport
          parallel.map{ i => arrayRingBuffer.enqueue(i)}
          parallel.map{ i => arrayRingBuffer.dequeue()}
      }
    }
  }

  performance of "immutable.RingBufferCAS" in {
    measure method "dequeue" in {
      using(ranges) in {
        r =>
          val ringBuffer = RingBufferCAS.empty[Int](r.size)
          val parallel = r.par
          parallel.tasksupport = taskSupport
          parallel.map{ i => ringBuffer.enqueue(i)}
          parallel.map{ i => ringBuffer.dequeue }
      }
    }
  }

  performance of "immutable.RingBufferSynchronized" in {
    measure method "dequeue" in {
      using(ranges) in {
        r =>
          val ringBuffer = RingBufferSynchronized.empty[Int](r.size)
          val parallel = r.par
          parallel.tasksupport = taskSupport
          parallel.map{ i => ringBuffer.enqueue(i)}
          parallel.map{ i => ringBuffer.dequeue}
      }
    }
  }

  performance of "ConcurrentLinkedQueue" in {
    measure method "dequeue" in {
      using(ranges) in {
        r =>
          var linkedQueue = new ConcurrentLinkedDeque[Int]()
          val parallel = r.par
          parallel.tasksupport = taskSupport
          parallel.map{ i =>
            linkedQueue.add(i)
          }
          parallel.map{ i =>
            linkedQueue.pop()
          }
      }
    }
  }

  performance of "LinkedBlockingQueue" in {
    measure method "dequeue" in {
      using(ranges) in {
        r =>
          var linkedQueue = new LinkedBlockingQueue[Int]()
          val parallel = r.par
          parallel.tasksupport = taskSupport
          parallel.map{ i =>
          linkedQueue.add(i)
        }
          parallel.map{ i =>
            linkedQueue.poll()
          }
      }
    }
  }

  performance of "ArrayBlockingQueue" in {
    measure method "dequeue" in {
      using(ranges) in {
        r =>
          var linkedQueue = new ArrayBlockingQueue[Int](r.size)
          val parallel = r.par
          parallel.tasksupport = taskSupport
          parallel.map{ i =>
            linkedQueue.add(i)
          }
          parallel.map{ i =>
            linkedQueue.poll
          }
      }
    }
  }

}
