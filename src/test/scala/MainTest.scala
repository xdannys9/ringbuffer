import java.util.concurrent.ConcurrentLinkedDeque

import immutable.{RingBufferCAS, RingBufferSynchronized}
import mutable.ArrayRingBufferCAS
import org.scalatest.FunSuite
import org.scalatest.concurrent.Eventually
import org.scalatest.time.{Millis, Seconds, Span}

class MainTest extends FunSuite with Eventually {

  override implicit val patienceConfig: PatienceConfig =
    PatienceConfig(timeout =  Span(3, Seconds), interval = Span(5, Millis))

  test("RingBuffer CAS enqueue and dequeue") {

    val list = List(1,2,3,4,5,6,7,8,9,10)

    val queue = RingBufferCAS.empty[Int](20)
    list.par.foreach{
      el =>
        queue.enqueue(el)
    }

    val concurrentLinkedQueue = new ConcurrentLinkedDeque[Int]()
    list.par.foreach{ _ =>
      val el = queue.dequeue
      if (el.isDefined) concurrentLinkedQueue.add(el.get)
    }

    eventually {
      assert(concurrentLinkedQueue.size==10)
      list.foreach{
        el =>
          assert(concurrentLinkedQueue.contains(el))
      }
    }
  }

  test("RingBuffer Synchronized enqueue and dequeue") {

    val list = List(1,2,3,4,5,6,7,8,9,10)

    val queue = RingBufferSynchronized.empty[Int](20)
    list.par.foreach{
      el =>
        queue.enqueue(el)
    }

    val concurrentLinkedQueue = new ConcurrentLinkedDeque[Int]()
    list.par.foreach{ _ =>
      val el = queue.dequeue
      if (el.isDefined) concurrentLinkedQueue.add(el.get)
    }

    eventually {
      assert(concurrentLinkedQueue.size==10)
      list.foreach{
        el =>
          assert(concurrentLinkedQueue.contains(el))
      }
    }
  }

  test("ArrayRingBuffer CAS enqueue and dequeue") {

    val list = List(1,2,3,4,5,6,7,8,9,10)

    val queue = ArrayRingBufferCAS.empty[Int](20)
    list.par.foreach{
      el =>
        queue.enqueue(el)

    }

    val concurrentLinkedQueue = new ConcurrentLinkedDeque[Int]()
    list.par.foreach{ _ =>
      val el = queue.dequeue()
      if (el.isDefined) concurrentLinkedQueue.add(el.get)
    }

    eventually {
      assert(concurrentLinkedQueue.size==10)
      list.foreach{
        el =>
          assert(concurrentLinkedQueue.contains(el))
      }
    }
  }
}
