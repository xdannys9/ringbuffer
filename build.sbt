lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "fi.metropolia",
      scalaVersion := "2.12.7"
    )),
    name := "ringbuffer"
  )

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % Test
libraryDependencies += "com.storm-enroute" % "scalameter_2.12" % "0.10.1" % Test